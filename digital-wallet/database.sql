CREATE TABLE Wallets(
    walletID Number(2,0) NOT NULL PRIMARY KEY,
    wallet_Name Varchar2(50) NOT NULL,
    cash Number(14,2) NOT NULL
);
CREATE TABLE NoteBridge(
    noteID Number(1,0) NOT NULL,
    walletID Number(2,0) NOT NULL,
    FOREIGN KEY(noteID) REFERENCES Notes(noteID),
    FOREIGN KEY(walletID) REFERENCES Wallets(walletID)
);
CREATE TABLE CardBridge(
    card_name Number(1,0) NOT NULL,
    walletID Number(2,0) NOT NULL,
    FOREIGN KEY(card_name) REFERENCES Cards(card_name),
    FOREIGN KEY(walletID) REFERENCES Wallets(walletID)
)
CREATE TABLE Notes(
    noteID Number(1,0) NOT NULL PRIMARY KEY,
    title Varchar2(25) NOT NULL,
    body Varchar2(500) NOT NULL,
    creation_Date Date() NOT NULL,
);
CREATE TABLE Cards(
    card_number Number(16,0) NOT NULL PRIMARY KEY,
    card_name Varchar2(25) NOT NULL,
    person_name Varchar2(25),
    expiry_date Date,
);
CREATE TABLE CreditCards(
    card_number Number(16,0) NOT NULL,
    three_Digits Number(3,0) NOT NULL,
    Credit_Limit Number(6,2) NOT NULL,
    credit_Debt Number(6,2) NOT NULL
    FOREIGN KEY(card_number) REFERENCES Cards(card_number),
);
CREATE TABLE DebitCards(
    card_number Number(16,0) NOT NULL,
    three_digits Number(3,0) NOT NULL,
    balance Number(6,2) NOT NULL,
    FOREIGN KEY(card_number) REFERENCES Cards(card_number),
);
CREATE TABLE PersonalCards(
    card_number Number(16,0) NOT NULL,
    FOREIGN KEY(card_number) REFERENCES Cards(card_number),
);