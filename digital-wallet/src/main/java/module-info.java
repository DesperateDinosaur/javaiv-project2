module com.digitalwallet {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires java.sql;

    opens com.digitalwallet to javafx.fxml;
    exports com.digitalwallet;
}
