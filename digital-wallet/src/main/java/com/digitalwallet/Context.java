package com.digitalwallet;

public class Context {
    private IChoice payMethod;

    public void setPayMethod(IChoice payMethod) {
        this.payMethod = payMethod;
    }

    public boolean deposit(double money){
        return this.payMethod.deposit(money);
    }

    public boolean withdraw(double money) {
        return this.payMethod.withdraw(money);
    }
}
