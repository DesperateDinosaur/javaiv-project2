package com.digitalwallet;

public class DebitCard extends Card implements IChoice{
    private int threeDigits;
    private double balance;
    private Bank bank;

    /**
     * constructor
     * @param crdName name of card
     * @param name card holders name
     * @param num card number
     * @param expire expiry date
     * @param threeD 3 digit security code
     * @param balance balance on the card
     */
    public DebitCard(String crdName, String name, String num, Date expire, int threeD, double balance){
        super(crdName, name, num, expire);
        this.threeDigits = threeD;
        this.balance = balance;
        this.bank = new Bank();
    }
    /**
     * copy constructor
     * @param card
     */
    public DebitCard(DebitCard card){
        this(card.getCrdName(), card.getName(), card.getNumber(), new Date(card.getExpiryDate()), card.getThreeDigits(), card.getBalance());
    }

    // get and set methods
    public double getBalance() {
        return balance;
    }
    public Bank getBank() {
        return bank;
    }
    public int getThreeDigits() {
        return threeDigits;
    }
    public void setThreeDigits(int threeDigits) {
        this.threeDigits = threeDigits;
    }

    // returns true if balance is greater or equal to amount
    public boolean checkFunds(int amount){
        return this.balance >= amount;
    }

    // balance - amount only if theres enough funds 
    // returns true if operation was successful
    public boolean withdraw(double money){
        if (! (this.balance - money >= 0)){
            this.balance = this.balance + money;
            return false;
        }
        try{
            bank.attemptTransaction();
            this.balance = this.balance - money;
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public boolean deposit(double money){
        return false;
    }


}