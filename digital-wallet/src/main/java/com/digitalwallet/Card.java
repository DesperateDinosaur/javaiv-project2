package com.digitalwallet;

/**
 * represents a singular unspecific card
 */
public class Card {
    protected String crdName;
    protected String name;
    protected String number;
    protected Date expiryDate;

    /**
     * constructor for a standart card
     * @param crdName
     * @param name
     * @param num
     * @param expire
     */
    public Card(String crdName, String name, String num, Date expire){
        this.crdName = crdName;
        this.name = name;
        this.number = num;
        
        Date newDate = new Date(expire.getDay(),expire.getMonth(),expire.getYear());
        this.expiryDate = newDate;
    }

    public String getCrdName() {
        return this.crdName;
    }
    public void setCrdName(String crdName) {
        this.crdName = crdName;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }

    public Date getExpiryDate() {
        return this.expiryDate;
    }
    public void setExpiryDate(Date expirydate) {
        this.expiryDate = expirydate;
    }
}
