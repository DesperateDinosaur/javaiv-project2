package com.digitalwallet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.awt.AWTException;
import java.awt.event.KeyEvent;
import java.awt.Robot;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * a controller object that handles the buttons and other functions in the cards scene
 */
public class cardsController implements Initializable {
    
    @FXML
    private TextField a;

    @FXML
    private TextField aCDExpMM;

    @FXML
    private TextField aCDExpYY;

    @FXML
    private TextField aCDCrdName;

    @FXML
    private TextField aCDName;

    @FXML
    private TextField aCDNum1;

    @FXML
    private TextField aCDNum2;

    @FXML
    private TextField aCDNum3;

    @FXML
    private TextField aCDNum4;

    @FXML
    private TextField aCDSec;

    @FXML
    private TextField aPCrdName;

    @FXML
    private TextField aPExpMM;

    @FXML
    private TextField aPExpYY;

    @FXML
    private TextField aPName;

    @FXML
    private TextField aPNum;

    @FXML
    private Button btnMode;

    @FXML
    private Button clearCredDeb;

    @FXML
    private Button clearGift;

    @FXML
    private Button clearPer;

    @FXML
    private Button createCredit;

    @FXML
    private Button createDebit;

    @FXML
    private Button createPer;

    @FXML
    private Button eCard0;

    @FXML
    private Button eCard1;

    @FXML
    private Button eCard2;

    @FXML
    private Button eCard3;

    @FXML
    private Button eCard4;

    @FXML
    private Button eCard5;

    @FXML
    private Button eCard6;

    @FXML
    private Button eCard7;

    @FXML
    private Button eCard8;

    @FXML
    private Button eCard9;

    @FXML
    private TextField eHName;

    @FXML
    private TextField eCName;

    @FXML
    private TextField eCNum;

    @FXML
    private TextField eExpMM;

    @FXML
    private TextField eExpYY;

    @FXML
    private TextField eSecNum;

    @FXML
    private Button eDel;

    @FXML
    private Button eSave;

    @FXML
    private ImageView imgMode;

    @FXML
    private Pane parent;

    @FXML
    private Button toMenu;

    @FXML
    private Button vCard0;

    @FXML
    private Button vCard1;

    @FXML
    private Button vCard2;

    @FXML
    private Button vCard3;

    @FXML
    private Button vCard4;

    @FXML
    private Button vCard5;

    @FXML
    private Button vCard6;

    @FXML
    private Button vCard7;

    @FXML
    private Button vCard8;

    @FXML
    private Button vCard9;

    @FXML
    private ImageView btnImg0;

    @FXML
    private ImageView btnImg1;

    @FXML
    private ImageView btnImg2;

    @FXML
    private ImageView btnImg3;

    @FXML
    private ImageView btnImg4;

    @FXML
    private ImageView btnImg5;

    @FXML
    private ImageView btnImg6;

    @FXML
    private ImageView btnImg7;

    @FXML
    private ImageView btnImg8;

    @FXML
    private ImageView btnImg9;

    @FXML
    private Label cDMsg;

    @FXML
    private Label pCMsg;

    private Image creditImg = new Image("file:src/main/resources/com/digitalwallet/images/credit.png");

    private Image debitImg = new Image("file:src/main/resources/com/digitalwallet/images/debit.png");

    private Image personalImg = new Image("file:src/main/resources/com/digitalwallet/images/personal.png");


    private boolean isDarkMode = true;
    private Button selected;
    private Button lastSelected;
    private int id;

    /**
     * responsible for going back to the main menu scene
     * @param event
     * @throws IOException
     */
    @FXML
    void exitToMain(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("main_menu");
    }

    /**
     * changes the theme from light mode to dark mode
     * @param event
     */
    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    /**
     * sets the theme to darkmode
     */
    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * sets the theme to light mode
     */
    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * method to implement actions after FXML loads 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buttonInitialize();
        Robot robot;
        try {
            robot = new Robot();
            aCDNum1.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 4){
                    aCDNum1.setText(oldValue);
                }
                if (newValue.length() == 4){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDNum1.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDNum2.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 4){
                    aCDNum2.setText(oldValue);
                }
                if (newValue.length() == 4){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDNum2.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDNum3.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 4){
                    aCDNum3.setText(oldValue);
                }
                if (newValue.length() == 4){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDNum3.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDNum4.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 4){
                    aCDNum4.setText(oldValue);
                }
                if (newValue.length() == 4){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDNum4.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDExpMM.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    aCDExpMM.setText(oldValue);
                }
                if (newValue.length() == 2){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDExpMM.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDExpYY.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    aCDExpYY.setText(oldValue);
                }
                if (newValue.length() == 2){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aCDExpYY.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aCDSec.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 3){
                    aCDSec.setText(oldValue);
                }
                if (!newValue.matches("\\d*")) {
                    aCDSec.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aPNum.textProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue.matches("\\d*")) {
                    aPNum.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aPExpMM.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    aPExpMM.setText(oldValue);
                }
                if (newValue.length() == 2){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    aPExpMM.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            aPExpYY.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    aPExpYY.setText(oldValue);
                }
                if (!newValue.matches("\\d*")) {
                    aPExpYY.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            eExpMM.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    eExpMM.setText(oldValue);
                }
                if (newValue.length() == 2){
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                }
                if (!newValue.matches("\\d*")) {
                    eExpMM.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            eExpYY.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 2){
                    eExpYY.setText(oldValue);
                }
                if (!newValue.matches("\\d*")) {
                    eExpYY.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
            eSecNum.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.length() > 3){
                    eSecNum.setText(oldValue);
                }
                if (!newValue.matches("\\d*")) {
                    eSecNum.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });

        } catch (AWTException e) {
            e.printStackTrace();
        }

        setCards();
        setEditcards();

    }

    /**
     * sets the cards on the scene so that they stay consistent between scenes
     */
    void setCards() {

            if (MainMenu.cards[0] == null) {
                vCard0.setText("");
                btnImg0.setImage(null);
            } else {
                vCard0.setText(MainMenu.cards[0].getCrdName());
                if (MainMenu.cards[0].getClass() == CreditCard.class) {
                    btnImg0.setImage(creditImg);
                } else if (MainMenu.cards[0].getClass() == DebitCard.class){
                    btnImg0.setImage(debitImg);
                }
                else {
                    btnImg0.setImage(personalImg);
                }
            }

            if (MainMenu.cards[1] == null) {
                vCard1.setText("");
                btnImg1.setImage(null);
            } else {
                vCard1.setText(MainMenu.cards[1].getCrdName());
                if (MainMenu.cards[1].getClass() == CreditCard.class) {
                    btnImg1.setImage(creditImg);
                } else if (MainMenu.cards[1].getClass() == DebitCard.class){
                    btnImg1.setImage(debitImg);
                }
                else {
                    btnImg1.setImage(personalImg);
                }
            }

            if (MainMenu.cards[2] == null) {
                vCard2.setText("");
                btnImg2.setImage(null);
            } else {
                vCard2.setText(MainMenu.cards[2].getCrdName());
                if (MainMenu.cards[2].getClass() == CreditCard.class) {
                    btnImg2.setImage(creditImg);
                } else if (MainMenu.cards[2].getClass() == DebitCard.class){
                    btnImg2.setImage(debitImg);
                }
                else {
                    btnImg2.setImage(personalImg);
                }
            }

            if (MainMenu.cards[3] == null) {
                vCard3.setText("");
                btnImg3.setImage(null);
            } else {
                vCard3.setText(MainMenu.cards[3].getCrdName());
                if (MainMenu.cards[3].getClass() == CreditCard.class) {
                    btnImg3.setImage(creditImg);
                } else if (MainMenu.cards[3].getClass() == DebitCard.class){
                    btnImg3.setImage(debitImg);
                }
                else {
                    btnImg3.setImage(personalImg);
                }
            }

            if (MainMenu.cards[4] == null) {
                vCard4.setText("");
                btnImg4.setImage(null);
            } else {
                vCard4.setText(MainMenu.cards[4].getCrdName());
                if (MainMenu.cards[4].getClass() == CreditCard.class) {
                    btnImg4.setImage(creditImg);
                } else if (MainMenu.cards[4].getClass() == DebitCard.class){
                    btnImg4.setImage(debitImg);
                }
                else {
                    btnImg4.setImage(personalImg);
                }
            }

            if (MainMenu.cards[5] == null) {
                vCard5.setText("");
                btnImg5.setImage(null);
            } else {
                vCard5.setText(MainMenu.cards[5].getCrdName());
                if (MainMenu.cards[5].getClass() == CreditCard.class) {
                    btnImg5.setImage(creditImg);
                } else if (MainMenu.cards[5].getClass() == DebitCard.class){
                    btnImg5.setImage(debitImg);
                }
                else {
                    btnImg5.setImage(personalImg);
                }
            }

            if (MainMenu.cards[6] == null) {
                vCard6.setText("");
                btnImg6.setImage(null);
            } else {
                vCard6.setText(MainMenu.cards[6].getCrdName());
                if (MainMenu.cards[6].getClass() == CreditCard.class) {
                    btnImg6.setImage(creditImg);
                } else if (MainMenu.cards[6].getClass() == DebitCard.class){
                    btnImg6.setImage(debitImg);
                }
                else {
                    btnImg6.setImage(personalImg);
                }
            }

            if (MainMenu.cards[7] == null) {
                vCard7.setText("");
                btnImg7.setImage(null);
            } else {
                vCard7.setText(MainMenu.cards[7].getCrdName());
                if (MainMenu.cards[7].getClass() == CreditCard.class) {
                    btnImg7.setImage(creditImg);
                } else if (MainMenu.cards[7].getClass() == DebitCard.class){
                    btnImg7.setImage(debitImg);
                }
                else {
                    btnImg7.setImage(personalImg);
                }
            }

            if (MainMenu.cards[8] == null) {
                vCard8.setText("");
                btnImg8.setImage(null);
            } else {
                vCard8.setText(MainMenu.cards[8].getCrdName());
                if (MainMenu.cards[8].getClass() == CreditCard.class) {
                    btnImg8.setImage(creditImg);
                } else if (MainMenu.cards[8].getClass() == DebitCard.class){
                    btnImg8.setImage(debitImg);
                }
                else {
                    btnImg8.setImage(personalImg);
                }
            }

            if (MainMenu.cards[9] == null) {
                vCard9.setText("");
                btnImg9.setImage(null);
            } else {
                vCard9.setText(MainMenu.cards[9].getCrdName());
                if (MainMenu.cards[9].getClass() == CreditCard.class) {
                    btnImg9.setImage(creditImg);
                } else if (MainMenu.cards[9].getClass() == DebitCard.class){
                    btnImg9.setImage(debitImg);
                }
                else {
                    btnImg9.setImage(personalImg);
                }
            }
    }

    /**
     * edits an indivisual card and updates the array in main menu
     */
    void setEditcards() {

            if (MainMenu.cards[0] == null) {
                eCard0.setText("");
            } else {
                eCard0.setText(MainMenu.cards[0].getCrdName());
            }

            if (MainMenu.cards[1] == null) {
                eCard1.setText("");
            } else {
                eCard1.setText(MainMenu.cards[1].getCrdName());
            }

            if (MainMenu.cards[2] == null) {
                eCard2.setText("");
            } else {
                eCard2.setText(MainMenu.cards[2].getCrdName());
            }

            if (MainMenu.cards[3] == null) {
                eCard3.setText("");
            } else {
                eCard3.setText(MainMenu.cards[3].getCrdName());
            }

            if (MainMenu.cards[4] == null) {
                eCard4.setText("");
            } else {
                eCard4.setText(MainMenu.cards[4].getCrdName());
            }

            if (MainMenu.cards[5] == null) {
                eCard5.setText("");
            } else {
                eCard5.setText(MainMenu.cards[5].getCrdName());
            }

            if (MainMenu.cards[6] == null) {
                eCard6.setText("");
            } else {
                eCard6.setText(MainMenu.cards[6].getCrdName());
            }

            if (MainMenu.cards[7] == null) {
                eCard7.setText("");
            } else {
                eCard7.setText(MainMenu.cards[7].getCrdName());
            }

            if (MainMenu.cards[8] == null) {
                eCard8.setText("");
            } else {
                eCard8.setText(MainMenu.cards[8].getCrdName());
            }

            if (MainMenu.cards[9] == null) {
                eCard9.setText("");
            } else {
                eCard9.setText(MainMenu.cards[9].getCrdName());
                
            }
    }

    /**
     * makes a credit card object and adds it to the cards array
     */
    @FXML
    public void makeCredit(){

        try {
            String crdName = aCDCrdName.getText();
            String name = aCDName.getText();
            String num = aCDNum1.getText() + aCDNum2.getText() + aCDNum3.getText() + aCDNum4.getText();

            int sMon = Integer.parseInt(aCDExpMM.getText());
            int sYear = Integer.parseInt(aCDExpYY.getText());

            if (crdName == null || name == null || num == null || aCDNum1.getText() == null || aCDNum2.getText() == null || aCDNum3.getText() == null || aCDNum4.getText() == null || aCDExpMM.getText() == null || aCDExpYY.getText() == null) {
                throw new Exception();
            }

            Date date = new Date(0, sMon, sYear);

            int code = Integer.parseInt(aCDSec.getText());

            for (int i = 0; i < MainMenu.cards.length; i++) {
                if (MainMenu.cards[i] == null) {
                    MainMenu.cards[i] = new CreditCard(crdName, name, num, date, code);
                    break;
                }
            }
            setCards();
            setEditcards();
            clearFields();
        } catch (Exception e) {
            cDMsg.setText("Please ensure all fields are filled in completely.");
        }

        
    }

    /**
     * makes a debit card object and adds it to the cards array
     */
    @FXML
    public void makeDebit(){
        
        try {
            String crdName = aCDCrdName.getText();
            String name = aCDName.getText();
            String num = aCDNum1.getText() + aCDNum2.getText() + aCDNum3.getText() + aCDNum4.getText();

            int sMon = Integer.parseInt(aCDExpMM.getText());
            int sYear = Integer.parseInt(aCDExpYY.getText());

            if (crdName == null || name == null || num == null || aCDNum1.getText() == null || aCDNum2.getText() == null || aCDNum3.getText() == null || aCDNum4.getText() == null) {
                throw new Exception();
            }

            Date date = new Date(0, sMon, sYear);

            int code = Integer.parseInt(aCDSec.getText());
            for (int i = 0; i < MainMenu.cards.length; i++) {
                if (MainMenu.cards[i] == null) {
                    MainMenu.cards[i] = new DebitCard(crdName, name, num, date, code, MainMenu.cash);
                    break;
                }
            }
            setCards();
            setEditcards();
            clearFields();
        }catch (Exception e) {
            cDMsg.setText("Please ensure all fields are filled in completely.");
        }

        
    }

    /**
     * makes a personal card object and adds it to the cards array
     */
    @FXML
    public void makePersonal(){
        
        try {
            String crdName = aPCrdName.getText();
            String name = aPName.getText();
            String num = aPNum.getText();

            if (crdName.isEmpty() || name.isEmpty() || num.isEmpty()) {
                throw new Exception();
            }

            String sMon = "";
            String sYear = "";

            int mon;
            int year;

            if (aPExpMM.getText().isEmpty() || aPExpYY.getText().isEmpty()) {
                mon = 0;
                year = 0;
            } else {
                sMon = aPExpMM.getText();
                sYear = aPExpYY.getText();
                mon = Integer.parseInt(sMon);
                year = Integer.parseInt(sYear);
            }

            Date date = new Date(0, mon, year);
            for (int i = 0; i < MainMenu.cards.length; i++) {
                if (MainMenu.cards[i] == null) {

                    if (sMon == null || sYear == null) {
                        MainMenu.cards[i] = new PersonalCard(crdName, name, num);
                        break;
                    } else {
                        MainMenu.cards[i] = new PersonalCard(crdName, name, num, date);
                        break;
                    } 
                }
            }
            setCards();
            setEditcards();
            clearFields();

        }catch (Exception e) {
            pCMsg.setText("Please ensure all required fields are filled in completely");
        }
    }

    /**
     * a button that allows the user to choose which card to edit
     * @param event
     */
    @FXML
    public void whichBtn(ActionEvent event) {
        selected = (Button) event.getSource();
        String name = selected.getId();
        String intValue = name.replaceAll("[^0-9]", "");
        id = Integer.parseInt(intValue);
        if(lastSelected != null) {

            lastSelected.setStyle("-fx-background-color: none");

        }

        try {
            eCName.setText(MainMenu.cards[id].getCrdName());
            eHName.setText(MainMenu.cards[id].getName());
            eHName.setText(MainMenu.cards[id].getName());
            eCNum.setText(MainMenu.cards[id].getNumber());
            if(MainMenu.cards[id].getExpiryDate() != null){
                String mon = Integer.toString(MainMenu.cards[id].getExpiryDate().getMonth());
                String year = Integer.toString(MainMenu.cards[id].getExpiryDate().getYear());
                eExpMM.setText(mon);
                eExpYY.setText(year);
            }
            if (MainMenu.cards[id].getClass() == CreditCard.class) {

                int sec = ((CreditCard) MainMenu.cards[id]).getThreeDigits();

                eSecNum.setText(Integer.toString(sec));
            }
            else if (MainMenu.cards[id].getClass() == DebitCard.class) {
                int sec = ((DebitCard) MainMenu.cards[id]).getThreeDigits();

                eSecNum.setText(Integer.toString(sec));
            }

        } catch (Exception e) {
            //do nothing
        }

        if(isDarkMode){
            selected.setStyle("-fx-background-color: #47354E");
        }
        else{
            selected.setStyle("-fx-background-color: #aed1d1");
        }

        lastSelected = selected;
    }

    /**
     * saves the changes made to a card object after editing
     */
    @FXML
    void saveCardChange() {
        MainMenu.cards[id].setCrdName(eCName.getText());
        MainMenu.cards[id].setName(eHName.getText());
        MainMenu.cards[id].setNumber(eCNum.getText());
        
        String sMon = "";
        String sYear = "";

        int mon;
        int year;

        if (aPExpMM.getText().isEmpty() || aPExpYY.getText().isEmpty()) {
            mon = 0;
            year = 0;

        } else {
            sMon = aPExpMM.getText();
            sYear = aPExpYY.getText();
            mon = Integer.parseInt(sMon);
            year = Integer.parseInt(sYear);
        }

        Date date = new Date(0, mon, year);

        MainMenu.cards[id].setExpiryDate(date);

        if (!eSecNum.getText().isEmpty()) {
            if (MainMenu.cards[id].getClass() == CreditCard.class) {
                ((CreditCard) MainMenu.cards[id]).setThreeDigits(Integer.parseInt(eSecNum.getText()));
            }
            else if (MainMenu.cards[id].getClass() == DebitCard.class) {
                ((DebitCard) MainMenu.cards[id]).setThreeDigits(Integer.parseInt(eSecNum.getText()));
                
            }
        }

        setCards();
        setEditcards();

    }

    /**
     * deletes a card object from the array
     */
    @FXML
    public void delCard() {
        MainMenu.cards[id] = null;

        

        setCards();
        setEditcards();
        clearFields();
    }

    public void clearFields(){
        aCDCrdName.clear();
        aCDExpMM.clear();
        aCDExpYY.clear();
        aCDName.clear();
        aCDNum1.clear();
        aCDNum2.clear();
        aCDNum3.clear();
        aCDNum4.clear();
        aCDSec.clear();
        aPCrdName.clear();
        aPExpMM.clear();
        aPExpYY.clear();
        aPName.clear();
        aPNum.clear();
        eCName.clear();
        eCNum.clear();
        eExpMM.clear();
        eExpYY.clear();
        eSecNum.clear();
        eHName.clear();
        cDMsg.setText("");
        pCMsg.setText("");
    }

    /**
     * initializes animations for buttons with backgrounds
     */
    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(clearCredDeb, Color.valueOf("#1a1a1a"), Color.valueOf("#5e5e5e"));
            AnimateButton.animateButton(createCredit, Color.valueOf("#1a1a1a"), Color.RED);
            AnimateButton.animateButton(createDebit, Color.valueOf("#1a1a1a"), Color.BLUE);
            AnimateButton.animateButton(createPer, Color.valueOf("#1a1a1a"), Color.GREY);
            AnimateButton.animateButton(clearPer, Color.valueOf("#1a1a1a"), Color.valueOf("#5e5e5e"));

        } else {
            AnimateButton.animateButton(clearCredDeb, Color.valueOf("#ead7d7"), Color.valueOf("#b2abd9"));
            AnimateButton.animateButton(createCredit, Color.valueOf("#ead7d7"), Color.valueOf("#ff2e2e"));
            AnimateButton.animateButton(createDebit, Color.valueOf("#ead7d7"), Color.CYAN);
            AnimateButton.animateButton(createPer, Color.valueOf("#ead7d7"), Color.GREY);
            AnimateButton.animateButton(clearPer, Color.valueOf("#ead7d7"), Color.valueOf("#b2abd9"));
        }
    }
}