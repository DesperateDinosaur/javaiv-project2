package com.digitalwallet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * controls the notes scene
 */
public class NotesController implements Initializable{

    @FXML
    private Button btnMode;

    @FXML
    private ImageView imgMode;

    @FXML
    private Button nDel;

    @FXML
    private Button nSave;

    @FXML
    private Button note0;

    @FXML
    private Button note1;

    @FXML
    private Button note2;

    @FXML
    private Button note3;

    @FXML
    private Button note4;

    @FXML
    private Button note5;

    @FXML
    private Button note6;

    @FXML
    private Button note7;

    @FXML
    private Button note8;

    @FXML
    private Button note9;

    @FXML
    private TextArea noteEdit;

    @FXML
    private Pane parent;

    @FXML
    private Button toMenu;

    private boolean isDarkMode = true;
    private Button selected;
    private Button lastSelected = new Button();
    private int id;

    /**
     * sets the scene to main menu
     * @param event
     * @throws IOException
     */
    @FXML
    void exitToMain(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("main_menu");
    }

    /**
     * switches theme from dark to light
     * @param event
     */
    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    /**
     * sets the theme to dark mode
     */
    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * sets the them to light mode
     */
    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * switches the view to the note button that was pressed
     * @param event
     */
    @FXML
    public void whichBtn(ActionEvent event) {
        selected = (Button) event.getSource();
        String name = selected.getId();
        String intValue = name.replaceAll("[^0-9]", "");
        id = Integer.parseInt(intValue);
        lastSelected.setStyle("-fx-background-color: none");

        try {
            noteEdit.setText(MainMenu.notes[id].getTitle()+ "\n" + MainMenu.notes[id].getBody());
        } catch (Exception e) {
            clearField();
        }

        if(isDarkMode){
            selected.setStyle("-fx-background-color: #47354E");
        }
        else{
            selected.setStyle("-fx-background-color: #aed1d1");
        }
        lastSelected = selected;
        noteEdit.requestFocus();
    }

    /**
     * saves note to a file
     * @throws IOException
     */
    @FXML
    public void saveNote() throws IOException {
        // System.out.println("note id: " + id);
        String content = noteEdit.getText();
        // System.out.println(content);
        MainMenu.notes[id].setBody(content);
        try {
            String title = MainMenu.notes[id].getTitle();
            
            if (title.isBlank()){
                title = "New Note";
            }
            selected.setText(title);
        } catch (Exception e) {
            //do nothing
        }
        MainMenu.btn[id] = selected;
        // System.out.println(MainMenu.btn[id]);
    }

    /**
     * outputs the text to a file
     * @throws IOException
     */
    @FXML
    public void outputNote() throws IOException {
        MainMenu.notes[id].outputFile();
    }

    /**
     * initializes actions after FXML loads
     */
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        buttonInitialize();
        note0.setText(MainMenu.btn[0].getText());
        note1.setText(MainMenu.btn[1].getText());
        note2.setText(MainMenu.btn[2].getText());
        note3.setText(MainMenu.btn[3].getText());
        note4.setText(MainMenu.btn[4].getText());
        note5.setText(MainMenu.btn[5].getText());
        note6.setText(MainMenu.btn[6].getText());
        note7.setText(MainMenu.btn[7].getText());
        note8.setText(MainMenu.btn[8].getText());
        note9.setText(MainMenu.btn[9].getText());
        note0.fire();
    }

    /**
     * clears the notes
     */
    @FXML
    public void clearField() {
        noteEdit.clear();
        selected.setText("New Note");
        MainMenu.notes[id].setBody("");
        MainMenu.btn[id] = selected;
    }

    /**
     * initializes animations for buttons with backgrounds
     */
    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(nSave, Color.valueOf("#145214"), Color.GREEN);
            AnimateButton.animateButton(nDel, Color.valueOf("#801616"), Color.RED);
        } else {
            AnimateButton.animateButton(nSave, Color.valueOf("#3fb13f"), Color.LIGHTGREEN);
            AnimateButton.animateButton(nDel, Color.valueOf("#c93939"), Color.SALMON);
        }
    }
}
