package com.digitalwallet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * controls the scene used to display cash
 */
public class CashController implements Initializable{

    @FXML
    private Pane parent;

    @FXML
    private Button addFunds;

    @FXML
    private Button btnMode;

    @FXML
    private ImageView imgMode;

    @FXML
    private Button toMenu;

    @FXML
    private Button payBtn;

    Cash cash = new Cash(MainMenu.cash);

    @FXML
    private Label cashDisplay;

    private boolean isDarkMode = true;

    /**
     * sets the scene to main menu
     * @param event
     * @throws IOException
     */
    @FXML
    void exitToMain(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("main_menu");
    }

    /**
     * changes the scene to the add_funds_view
     * @param event
     * @throws IOException
     */
    @FXML
    void toAddFunds(ActionEvent event) throws IOException{
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("add_funds_view");
    }

    /**
     * changes the scene to the pay_view
     * @param event
     * @throws IOException
     */
    @FXML
    void toPay(ActionEvent event) throws IOException{
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("pay_view");
    }

    /**
     * changes theme from light mode to dark mode
     * @param event
     */
    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    /**
     * sets the scene to dark mode
     */
    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * sets the scene to light mode
     */
    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * initialized actions after the FXML loads
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setCash();
        buttonInitialize();
    }

    /**
     * sets the cash from the amount in the main menu "database"
     */
    void setCash(){
        cashDisplay.setText("$" + Double.toString(cash.getCash()));
        MainMenu.cash = cash.getCash();
    }

    /**
     * initializes animations for buttons with backgrounds
     */
    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(addFunds, Color.valueOf("#145214"), Color.GREEN);
            AnimateButton.animateButton(payBtn, Color.valueOf("#801616"), Color.RED);
        } else {
            AnimateButton.animateButton(addFunds, Color.valueOf("#3fb13f"), Color.LIGHTGREEN);
            AnimateButton.animateButton(payBtn, Color.valueOf("#c93939"), Color.SALMON);
        }
    }
}
