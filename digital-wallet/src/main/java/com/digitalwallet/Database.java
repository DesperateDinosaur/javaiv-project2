package com.digitalwallet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.scene.control.Button;


public class Database {
    private Connection conn;
    private int walletid;
    private Note[] notes;
    private Card[] cards;

    public Database(){
        // gets connected
        try{
            this.conn = getConnection("a2032587", "response420");
            System.out.println("Successfully connected");
        } catch (SQLException e){
            System.out.println("Could not establish connect for some reason" + e);
        }
    } 
 
    public void recreateDatabase(){
        // drop tables
        try {
            dropTable();
            System.out.println("Tables dropped");
        } catch (Exception e) {
            System.out.println("Could not drop tables" + e);
        }
        // creates tables
        try {
            createTable();
            System.out.println("Tables made");
        } catch (Exception e) {
            System.out.println("Could not create tables" + e);
        }
    }

    public double load(Note[] oldNotes, Card[] oldCards, Button[] buttons){
        double cash = 0;
        try {
            loadNotes(oldNotes);
            for(int i = 0; i < buttons.length; i++){
                if(oldNotes[i].getTitle() == ""){
                    buttons[i].setText("New Note");
                } else{
                    buttons[i].setText(oldNotes[i].getTitle());
                }
            }
            loadCards(oldCards);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT cash FROM wallets");
            while(rs.next()){
                cash = rs.getDouble("cash");
            }
        } catch (SQLException e) {
            System.out.println("couldnt fetch from the database " + e);
        }
        return cash;
    }
    private void loadNotes(Note[] oldNotes) throws SQLException{
        String selectString = "SELECT * FROM Notes";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(selectString);
        int i = 0;
        while(rs.next()){
            Note newNote = new Note();
            if(rs.getString("title") != null && rs.getString("body") != null){
                newNote.setBody(rs.getString("title") + "\n" + rs.getString("body"));
            } else if (rs.getString("title") != null && rs.getString("body") == null){
                newNote.setBody(rs.getString("title") + "\n" + "");
            }
            oldNotes[i] = newNote;
            i++;
         }
    }
    private void loadCards(Card[] oldCards) throws SQLException{
        String cardQuery = "SELECT * FROM Cards";
        Statement stmt = conn.createStatement();
        PreparedStatement debitPrepare = conn.prepareStatement("SELECT * FROM debitcards WHERE card_number = ?");
        PreparedStatement creditPrepare = conn.prepareStatement("SELECT * FROM creditcards WHERE card_number = ?");
        PreparedStatement personalPrepare = conn.prepareStatement("SELECT * FROM personalcards WHERE card_number = ?");
        ResultSet rs = stmt.executeQuery(cardQuery);
        int i = 0;
        while(rs.next()){
            String cardnumber = rs.getString("card_number");
            String cardname = rs.getString("card_name");
            String person = rs.getString("person_name");
            String expiry = rs.getString("expiry_date");
            debitPrepare.setString(1, cardnumber);
            ResultSet debitrs = debitPrepare.executeQuery();
            while(debitrs.next()){
                // date needs to be modified
                DebitCard newdc = new DebitCard(cardname, person, cardnumber, new Date(expiry), debitrs.getInt("three_digits"), debitrs.getDouble("balance"));
                oldCards[i] = newdc;
            }
            creditPrepare.setString(1, cardnumber);
            ResultSet creditrs = creditPrepare.executeQuery();
            while (creditrs.next()) {
                // date needs to be modified
                CreditCard newcc = new CreditCard(cardname, person, cardnumber, new Date(expiry), creditrs.getInt("three_digits"), creditrs.getDouble("credit_limit"), creditrs.getDouble("CREDIT_DEBT"));
                oldCards[i] = newcc;
            }
            personalPrepare.setString(1, cardnumber);
            ResultSet personalrs = personalPrepare.executeQuery();
            while (personalrs.next()) {
                PersonalCard newpc;
                if(rs.getString("expiry_date")=="0/0/0"){
                    newpc = new PersonalCard(cardname, person, cardnumber);
                }
                // date needs to be modified
                newpc = new PersonalCard(cardname, person, cardnumber, new Date(expiry));
                oldCards[i] = newpc;
            }
            i++;
        }
    }
    public void save(Note[] notes, Card[] cards, int walletid, String walletname, double cash){
        recreateDatabase();
        // supposed to be called once and calls other helper methods
        // Note[] notes = new Note[2];
        // notes[0] = new Note();
        // notes[1] = new Note();
        // notes[0].setBody("this is note number 1 \n this is a new line");
        // notes[1].setBody("this is note number 2 \n this is also a new line");
        // Card[] cards = new Card[3];
        // cards[0] = new DebitCard("mydebit", "kelsey", "1010101010101010", new Date(1, 2, 3), 125, 100.00);
        // cards[1] = new CreditCard("mycredit", "kelsey", "1235842125413698", new Date(8, 2, 3), 125);
        // cards[2] = new PersonalCard("mypersonal", "kelsey", "1254785445874587");
        try {
            saveNotes(notes);
            saveWallet(walletid, walletname, cash);
            bridgeNotesWallets();
            saveCards(cards);
            bridgeCardsWallets();
            System.out.println("Successfully inserted into database");
        } catch (SQLException e) {
            System.out.println("Could not insert into database " + e);
        }

    }
    private void bridgeCardsWallets() throws SQLException{
        String insertString = "INSERT INTO cardbridge(card_number, walletid)" + 
                                "VALUES(?,?)";
        PreparedStatement stmt = conn.prepareStatement(insertString);
        for(int i = 0; i < this.cards.length; i++){
            if (cards[i] != null) {
                stmt.setString(1, cards[i].getNumber());
                stmt.setInt(2, this.walletid);
                stmt.addBatch();
            }
        }
        stmt.executeBatch();
    }
    private void saveCards(Card[] cards) throws SQLException{
        String insertCards = "INSERT INTO cards(card_number,card_name,person_name,expiry_date)" +
            "VALUES(?,?,?,?)";
        String insertdebit = "INSERT INTO debitcards(card_number,three_digits,balance)" +
            "VALUES(?,?,?)";
        String insertcredit = "INSERT INTO creditcards(card_number,three_digits,credit_limit,credit_debt)" +
            "VALUES(?,?,?,?)";
        String insertpersonal = "INSERT INTO personalcards(card_number)" +
            "VALUES(?)";
        PreparedStatement basecards = conn.prepareStatement(insertCards);
        PreparedStatement basedebit = conn.prepareStatement(insertdebit);
        PreparedStatement basecredit = conn.prepareStatement(insertcredit);
        PreparedStatement basepersonal = conn.prepareStatement(insertpersonal);
        for (int i = 0; i < cards.length; i++){
            if (cards[i] != null) {
                basecards.setString(1, cards[i].getNumber());
                basecards.setString(2, cards[i].getCrdName());
                basecards.setString(3, cards[i].getName());
                basecards.setString(4, cards[i].getExpiryDate().formatDDMMYY());
                basecards.addBatch();
                if(cards[i] instanceof DebitCard) {
                    basedebit.setString(1, cards[i].getNumber());
                    basedebit.setInt(2, ((DebitCard)cards[i]).getThreeDigits());
                    basedebit.setDouble(3, ((DebitCard)cards[i]).getBalance());
                    basedebit.addBatch();
                } else if (cards[i] instanceof CreditCard){
                    basecredit.setString(1, cards[i].getNumber());
                    basecredit.setInt(2, ((CreditCard)cards[i]).getThreeDigits());
                    basecredit.setDouble(3, ((CreditCard)cards[i]).getLimit());
                    basecredit.setDouble(4, ((CreditCard)cards[i]).getAmount());
                    basecredit.addBatch();
                } else {
                    basepersonal.setString(1, cards[i].getNumber());
                    basepersonal.addBatch(); 
                } 
            }
            else {
                break;
            }
        }
        basecards.executeBatch();
        basedebit.executeBatch();
        basecredit.executeBatch();
        basepersonal.executeBatch();
        this.cards = cards;
    }
    private void bridgeNotesWallets() throws SQLException{
        String insertString = "INSERT INTO notebridge(noteid, walletid)" + 
                                "VALUES(?,?)";
        PreparedStatement stmt = conn.prepareStatement(insertString);
        for(int i = 0; i < this.notes.length; i++){
            stmt.setInt(1, i);
            stmt.setInt(2, this.walletid);
            stmt.addBatch();
        }
        stmt.executeBatch();
    }
    private void saveNotes(Note[] notes) throws SQLException{
        String insertString = "INSERT INTO notes(noteid, title, body, creation_date)" + 
                                "VALUES(?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(insertString);
        for(int i = 0; i < notes.length; i++){
            stmt.setInt(1, i);
            stmt.setString(2, notes[i].getTitle());
            stmt.setString(3, notes[i].getBody());
            stmt.setString(4, notes[i].getCreationDate().formatDDMMYY());

            stmt.addBatch();
        }
        stmt.executeBatch();
        this.notes = notes;
    }
    private void saveWallet(int id, String name, double cash) throws SQLException{
        String insertString = "INSERT INTO wallets(walletid, wallet_name, cash)" + 
                                "VALUES(?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(insertString);
        stmt.setInt(1, id);
        stmt.setString(2, name);
        stmt.setDouble(3, cash);
        stmt.execute();
        this.walletid = id;
    }

    private Connection getConnection(String username, String password) throws SQLException{
        String url = "jdbc:oracle:thin:@pdbora19c.dawsoncollege.qc.ca:1521/pdbora19c.dawsoncollege.qc.ca";
        return DriverManager.getConnection(url, username, password);
    }

    private void dropTable() throws SQLException{
        String dropCred = "DROP TABLE CreditCards";
        String dropDeb = "DROP TABLE DebitCards";
        String dropPer = "DROP TABLE PersonalCards" ;
        String dropNoteBrid = "DROP TABLE NoteBridge" ;
        String dropCardBrid = "DROP TABLE CardBridge" ;
        String dropWall = "DROP TABLE Wallets" ;
        String dropNote = "DROP TABLE Notes" ;
        String dropCards = "DROP TABLE Cards";

        Statement stmt = conn.createStatement();
        stmt.addBatch(dropCred);
        stmt.addBatch(dropDeb);
        stmt.addBatch(dropPer);
        stmt.addBatch(dropNoteBrid);
        stmt.addBatch(dropCardBrid);
        stmt.addBatch(dropWall);
        stmt.addBatch(dropNote);
        stmt.addBatch(dropCards);
        stmt.executeBatch();
    }
    
    private void createTable() throws SQLException {
        String walletString = "CREATE TABLE Wallets(walletID Number(2,0) NOT NULL PRIMARY KEY, wallet_Name Varchar2(50) NOT NULL, cash Number(14,2) NOT NULL)";
        String notesString = "CREATE TABLE Notes(noteID Number(1,0) NOT NULL PRIMARY KEY,title Varchar2(25),body Varchar2(500),creation_Date varchar2(20) NOT NULL)";
        String cardsString = "CREATE TABLE Cards(card_number varchar2(16) NOT NULL PRIMARY KEY,card_name Varchar2(25) NOT NULL,person_name Varchar2(25),expiry_date varchar2(20))";
        String creditString = "CREATE TABLE CreditCards(card_number varchar2(16) NOT NULL,three_Digits Number(3,0) NOT NULL,Credit_Limit Number(6,2) NOT NULL,credit_Debt Number(6,2) NOT NULL,FOREIGN KEY(card_number) REFERENCES Cards(card_number))";
        String debitString = "CREATE TABLE DebitCards(card_number varchar2(16) NOT NULL,three_digits Number(3,0) NOT NULL,balance Number(6,2) NOT NULL,FOREIGN KEY(card_number) REFERENCES Cards(card_number))";
        String personalString = "CREATE TABLE PersonalCards(card_number varchar2(16) NOT NULL,FOREIGN KEY(card_number) REFERENCES Cards(card_number))";
        String noteBridge = "CREATE TABLE NoteBridge(noteID Number(1,0) NOT NULL,walletID Number(2,0) NOT NULL,FOREIGN KEY(noteID) REFERENCES Notes(noteID),FOREIGN KEY(walletID) REFERENCES Wallets(walletID))";
        String cardBridge = "CREATE TABLE CardBridge(card_number varchar2(16) NOT NULL,walletID Number(2,0) NOT NULL,FOREIGN KEY(card_number) REFERENCES Cards(card_number),FOREIGN KEY(walletID) REFERENCES Wallets(walletID))";
        Statement stmt = conn.createStatement();
        stmt.addBatch(walletString);
        stmt.addBatch(notesString);
        stmt.addBatch(cardsString);
        stmt.addBatch(creditString);
        stmt.addBatch(debitString);
        stmt.addBatch(personalString);
        stmt.addBatch(noteBridge);
        stmt.addBatch(cardBridge);
        stmt.executeBatch();
      }
}