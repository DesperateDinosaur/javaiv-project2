package com.digitalwallet;
import java.util.Random;

/**
 * A mock object that simulates a bank
 */
public class Bank {
    Random randy = new Random();
    
    /**
     * randomly generates a 1 in 4 chance to simulate a bank transaction error
     */
    public void attemptTransaction(){
        int randomInt = randy.nextInt(100) + 1;
        if (randomInt <=25){
            throw new IllegalArgumentException();
        }
    }
}
