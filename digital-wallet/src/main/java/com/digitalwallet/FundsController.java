package com.digitalwallet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * controls the scene for funds
 */
public class FundsController implements Initializable {

    @FXML
    private Button addFunds;

    @FXML
    private Button btnMode;

    @FXML
    private ImageView imgMode;

    @FXML
    private Pane parent;

    @FXML
    private TextField dol_amnt;

    @FXML
    private TextField cen_amnt;

    @FXML
    private Label msg;

    @FXML
    private Button toMenu;

    Cash cash = new Cash(MainMenu.cash);
    private boolean isDarkMode = true;
    Context con = new Context();

    /**
     * sets the scene to main menu
     * @param event
     * @throws IOException
     */
    @FXML
    void exitToMain(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("main_menu");
    }

    /**
     * sets the scene to cash_view
     * @param event
     * @throws IOException
     */
    @FXML
    void toCash(ActionEvent event) throws IOException{
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("cash_view");
    }

    /**
     * changes theme from light to dark
     * @param event
     */
    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    /**
     * sets the theme to dark mode
     */
    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * sets the theme to light mode
     */
    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * initializes actions after the FXML loads
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buttonInitialize();
        dol_amnt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                dol_amnt.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        cen_amnt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 2){
                cen_amnt.setText(oldValue);
            }
            if (!newValue.matches("\\d*")) {
                cen_amnt.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    /**
     * adds money to the cash object in main menu "database"
     */
    @FXML
    public void addFunds() {
        Double total = Double.parseDouble(dol_amnt.getText() + "." + cen_amnt.getText());
        con.setPayMethod(cash);
        cash.deposit(total);
        msg.setText("$" + total + " was added to your account");
        dol_amnt.setText("");
        cen_amnt.setText("");
        MainMenu.cash = cash.getCash();
    }

    /**
     * initializes animations for buttons with backgrounds
     */
    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(addFunds, Color.valueOf("#145214"), Color.GREEN);
        } else {
            AnimateButton.animateButton(addFunds, Color.valueOf("#3fb13f"), Color.LIGHTGREEN);
        }
    }
}
