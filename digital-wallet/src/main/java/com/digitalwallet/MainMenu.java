package com.digitalwallet;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.scene.Parent;

/**
 * holds the objects like a database and runs the application
 */
public class MainMenu extends Application{
    
    public static Scene scene;
    public static String styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
    public static Boolean darkOn = true;
    public static double cash = 500;
    public static Note[] notes = {new Note(), new Note(), new Note(), new Note(), new Note(), new Note(),
                                     new Note(), new Note(), new Note(), new Note()};
    public static Button[] btn = {new Button("New Note"), new Button("New Note"), new Button("New Note"),
                                     new Button("New Note"), new Button("New Note"), new Button("New Note"),
                                     new Button("New Note"), new Button("New Note"), new Button("New Note"),
                                     new Button("New Note")};
    public static Card[] cards = new Card[10];

    @Override
    public  void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("main_menu"), 800, 500);
        scene.getStylesheets().remove(styleMode);
        scene.getStylesheets().add(styleMode);
        stage.getIcons().add(new Image(getClass().getResource("resources/images/icon.png").toExternalForm()));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Wall-E");
        stage.show();

    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainMenu.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        // Note[] notes = new Note[2];
        // notes[0] = new Note();
        // notes[1] = new Note();
        // notes[0].setBody("this is note number 1 \n this is a new line");
        // notes[1].setBody("this is note number 2 \n this is also a new line");
        // Database db = new Database();
        // System.out.println("before note change: " + notes[0].getBody());
        // db.save(notes);
        // notes[0].setBody("this is a new note string and \n should not be saved");
        // System.out.println("after note change: " + notes[0].getBody());
        // db.load(notes);
        // System.out.println("after load: " + notes[0].getBody());
        launch();
    }

}
