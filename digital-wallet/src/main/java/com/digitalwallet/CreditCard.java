package com.digitalwallet;
import java.util.Random;

public class CreditCard extends Card implements IChoice{
    private int threeDigits;
    private double limit;
    private double amount;
    private Bank bank;
    private final int MAX_LIMIT = 5000;
    private final int MIN_LIMIT = 100;

    /**
     * constructor for standart credit card
     * @param crdName
     * @param name
     * @param num
     * @param exp
     * @param threeDigits
     */
    public CreditCard(String crdName, String name, String num, Date exp, int threeDigits){
        super(crdName, name, num, exp);
        this.threeDigits = threeDigits;
        this.limit = setLimit();
        this.amount = this.limit / 2;
        this.bank = new Bank();
    }
    public CreditCard(String crdName, String name, String num, Date exp, int threeDigits, double limit, double amount){
        this(crdName, name,num,new Date(exp),threeDigits);
        this.limit = limit;
        this.amount = amount;
    }
    /**
     * copy constructor
     * @param card
     */
    public CreditCard(CreditCard card){
        this(card.getCrdName(), card.getName(),card.getNumber(),new Date(card.getExpiryDate()),card.getThreeDigits());
    }

    // get and set methods
    public double getAmount() {
        return amount;
    }
    public Bank getBank() {
        return bank;
    }
    public double getLimit() {
        return limit;
    }
    public int getThreeDigits() {
        return threeDigits;
    }
    public void setThreeDigits(int threeDigits) {
        this.threeDigits = threeDigits;
    }

    // sets a random limit between 100-5000
    private int setLimit(){
        Random randy = new Random();
        return randy.nextInt(MAX_LIMIT - MIN_LIMIT) + MIN_LIMIT;
    }

    // puts money into credit card making sure it doesnt pass the limit
    public boolean deposit(double amount){
        this.amount = this.amount + amount;
        if ( ! (this.amount >0 && this.amount < this.limit)){
            this.amount = this.amount - amount;
            return false;
        }
        try{
            bank.attemptTransaction();
            return true;
        } catch (IllegalArgumentException e) {
            this.amount = this.amount - amount;
            return false;
        }
    }

    // takes money out of amount making sure it doesnt go past half the limit
    public boolean withdraw(double amount){
        if (this.amount < amount){
            return false;
        }
        this.amount = this.amount - amount;
        try{
            bank.attemptTransaction();
            return true;
        } catch (IllegalArgumentException e) {
            this.amount = this.amount + amount;
            return false;
        }
    }
}