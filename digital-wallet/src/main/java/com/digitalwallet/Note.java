package com.digitalwallet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Note{
  private String title;
  private String body;
  private Date creationDate;
  private int id;

  /**
   * note constructor
   */
  public Note(){
    Calendar cal = Calendar.getInstance();
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int month = cal.get(Calendar.MONTH);
    int year = cal.get(Calendar.YEAR);
    this.creationDate = new Date(day, month, year);
    this.title = "";
    this.body = "";
  }

  /**
   * copy constructor
   * @param n the note object to be copies from
   */
  public Note(Note n){
    this.title = n.getTitle();
    this.body = n.getBody();
  }

  public Date getCreationDate() {
      return creationDate;
  }
  /**
   * gets note title
   * @return title of note
   */
  public String getTitle(){
    return this.title;
  }
  /**
   * sets the title of the note 
   * @param title
   */
  private void setTitle(String title){
    this.title = title;
  }
  public String getBody(){
    return this.body;
  }

  /**
   * sets the body of the note, if there is no body and only a title then sets that line as the title
   * @param body
   */
  public void setBody(String body){
    if (!body.isEmpty()){
      try {
        String[] strings = body.split("\n", 2);
        while (strings[0].trim().isEmpty()){
          strings = strings[1].split("\n", 2);
        }
        try {
          this.body = strings[1];
        } catch (Exception e) {
          //ignore
        }
        setTitle(strings[0]);
      } catch (Exception e) {
        this.body = "";
        setTitle(body);
      }
    } else {
      this.body = "";
      setTitle("");
    }
  }

  /**
   * outputs the note to a txt file
   * @throws IOException
   */
  public void outputFile() throws IOException{
    JFrame parentFrame = new JFrame();

    String cleanTitle = this.title.replaceAll("[^a-zA-Z0-9\\-]", "");

    System.out.println(cleanTitle);

    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setDialogTitle("Save note");
    fileChooser.setSelectedFile(new File(cleanTitle + ".txt"));
    fileChooser.setFileFilter(new FileNameExtensionFilter("text file","txt"));

    int userSelection = fileChooser.showSaveDialog(parentFrame);

    if (userSelection == JFileChooser.APPROVE_OPTION){
      File note = fileChooser.getSelectedFile();
      FileWriter noteWriter = new FileWriter(note);
      noteWriter.write(this.title + "\n");
      noteWriter.write(this.body);
      noteWriter.close();
      // System.out.println("Save as file: " + note.getAbsolutePath());
    }
  }
}