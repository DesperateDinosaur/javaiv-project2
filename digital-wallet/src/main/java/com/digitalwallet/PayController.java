package com.digitalwallet;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class PayController implements Initializable{

    @FXML
    private Button addFunds;

    @FXML
    private Button btnMode;

    @FXML
    private ImageView imgMode;

    @FXML
    private TextField pAmnt;

    @FXML
    private Button payCard;

    @FXML
    private Button payCash;

    @FXML
    private Button pCard0;

    @FXML
    private Button pCard1;

    @FXML
    private Button pCard2;

    @FXML
    private Button pCard3;

    @FXML
    private Button pCard4;

    @FXML
    private Button pCard5;

    @FXML
    private Button pCard6;

    @FXML
    private Button pCard7;

    @FXML
    private Button pCard8;

    @FXML
    private Button pCard9;

    @FXML
    private Pane parent;

    @FXML
    private Button toMenu;

    @FXML
    private Label paymentMsg;

    private boolean isDarkMode = true;

    private Button selected;

    private Button lastSelected;

    private int id;

    Cash cash = new Cash(MainMenu.cash);
    Context con = new Context();


    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    @FXML
    void exitToMain(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("main_menu");
    }

    @FXML
    void toAddFunds(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("add_funds_view");
    }

    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        buttonInitialize();
        setCards();
        
    }

    void setCards() {

        if (MainMenu.cards[0] == null) {
            pCard0.setText("");
        } else {
            pCard0.setText(MainMenu.cards[0].getCrdName());
        }

        if (MainMenu.cards[1] == null) {
            pCard1.setText("");
        } else {
            pCard1.setText(MainMenu.cards[1].getCrdName());
        }

        if (MainMenu.cards[2] == null) {
            pCard2.setText("");
        } else {
            pCard2.setText(MainMenu.cards[2].getCrdName());
        }

        if (MainMenu.cards[3] == null) {
            pCard3.setText("");
        } else {
            pCard3.setText(MainMenu.cards[3].getCrdName());
        }

        if (MainMenu.cards[4] == null) {
            pCard4.setText("");
        } else {
            pCard4.setText(MainMenu.cards[4].getCrdName());
        }

        if (MainMenu.cards[5] == null) {
            pCard5.setText("");
        } else {
            pCard5.setText(MainMenu.cards[5].getCrdName());
        }

        if (MainMenu.cards[6] == null) {
            pCard6.setText("");
        } else {
            pCard6.setText(MainMenu.cards[6].getCrdName());
        }

        if (MainMenu.cards[7] == null) {
            pCard7.setText("");
        } else {
            pCard7.setText(MainMenu.cards[7].getCrdName());
        }

        if (MainMenu.cards[8] == null) {
            pCard8.setText("");
        } else {
            pCard8.setText(MainMenu.cards[8].getCrdName());
        }

        if (MainMenu.cards[9] == null) {
            pCard9.setText("");
        } else {
            pCard9.setText(MainMenu.cards[9].getCrdName());
        }
    }

    public void whichBtn(ActionEvent event) {
        selected = (Button) event.getSource();
        String name = selected.getId();
        String intValue = name.replaceAll("[^0-9]", "");
        id = Integer.parseInt(intValue);
        if(lastSelected != null) {

            lastSelected.setStyle("-fx-background-color: none");

        }

        if(isDarkMode){
            selected.setStyle("-fx-background-color: #47354E");
        }
        else{
            selected.setStyle("-fx-background-color: #aed1d1");
        }

        lastSelected = selected;
    }

    @FXML
    public void payCard(ActionEvent event) {

        try {

            if (MainMenu.cards[id].getClass() == CreditCard.class) {
                con.setPayMethod((CreditCard)MainMenu.cards[id]);

                if (con.withdraw(Integer.parseInt(pAmnt.getText()))){

                    con.withdraw(Integer.parseInt(pAmnt.getText()));
                    paymentMsg.setText("$" + pAmnt.getText() + " was paid using " + MainMenu.cards[id].getCrdName());
                }
                else {
                    paymentMsg.setText("You are trying to pay over your card limit, enter a lower amount to pay");
                }
            }
            else if (MainMenu.cards[id].getClass() == DebitCard.class) {
                con.setPayMethod((DebitCard)MainMenu.cards[id]);
                
                if(((DebitCard) MainMenu.cards[id]).checkFunds(Integer.parseInt(pAmnt.getText()))) {

                    con.withdraw(Integer.parseInt(pAmnt.getText()));
                    paymentMsg.setText("$" + pAmnt.getText() + " was paid using " + MainMenu.cards[id].getCrdName());
                    MainMenu.cash = ((DebitCard) MainMenu.cards[id]).getBalance();
                }
                else {
                    paymentMsg.setText("Please add more funds to account to make this payment");
                }
            }
            else {
                paymentMsg.setText("Please choose a valid card or use the 'Pay Cash' option");
            }

            if (MainMenu.cash < 30) {
                paymentMsg.setText("You have less than $30, consider adding funds before next payment");
            }
            else {
                paymentMsg.setText("");
            }

            pAmnt.clear();

        } catch(Exception e) {
            paymentMsg.setText("Please ensure all fields are filled out or that you have choosen a valid card");
        }
    }

    @FXML
    public void payCash(ActionEvent event) {
        try {
            con.setPayMethod(cash);

            if (con.withdraw(Double.parseDouble(pAmnt.getText())) == true) {                

                // cash.withdraw(Double.parseDouble(pAmnt.getText()));
                paymentMsg.setText("$" + pAmnt.getText() + " was paid using cash");
                MainMenu.cash = cash.getCash();
                
            } else {

                paymentMsg.setText("Please add funds to make this payment");
                
            }

            if (MainMenu.cash < 30) {
                paymentMsg.setText("You have less than $30, consider adding funds before next payment");
            }
            else {
                paymentMsg.setText("");
            }

            pAmnt.clear();

        } catch (Exception e) {

            paymentMsg.setText("Please ensure all fields are filled out");
        }
    }

    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(payCard, Color.valueOf("#801616"), Color.RED);
            AnimateButton.animateButton(payCash, Color.valueOf("#801616"), Color.RED);
            AnimateButton.animateButton(addFunds, Color.valueOf("#145214"), Color.GREEN);

        } else {
            AnimateButton.animateButton(payCard, Color.valueOf("#c93939"), Color.SALMON);
            AnimateButton.animateButton(payCash, Color.valueOf("#c93939"), Color.SALMON);
            AnimateButton.animateButton(addFunds, Color.valueOf("#3fb13f"), Color.LIGHTGREEN);
        }
    }





}
