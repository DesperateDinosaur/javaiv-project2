package com.digitalwallet;

import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class Greeting implements Runnable {
    private Thread t;
    private String threadName;
    private Random rand = new Random();
    private Label greetingText;

    /**
    * constructor
    * @param greetingText text label on main menu to be animated
    */
    public Greeting(Label greetingText){
        this.threadName = "greetingThread";
        this.greetingText = greetingText;
        setGreeting();
    }

    /**
    * runs the greeting on a thread
    */
    public void run() {
        Text text = new Text(greetingText.getText());
        new Scene(new Group(text));
        text.setFont(Font.font("System", 36));
        double greetWidth = text.getLayoutBounds().getWidth();
        System.out.println(greetWidth);

        KeyValue initKeyValue = new KeyValue(greetingText.translateXProperty(), 800+greetWidth);
        KeyFrame initFrame = new KeyFrame(Duration.ZERO, initKeyValue);

        KeyValue endKeyValue = new KeyValue(greetingText.translateXProperty(), -1.0
        * greetWidth);
        KeyFrame endFrame = new KeyFrame(Duration.seconds(10), endKeyValue);

        Timeline timeline = new Timeline(initFrame, endFrame);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
    * starts the thread
    */
    public void start() {
        if (t == null){
            t = new Thread (this, threadName);
            t.start();
        }
    }

    private void setGreeting(){
        int choice = rand.nextInt(5);

        switch(choice){
            case 0:
            this.greetingText.setText("Greetings earthling! We have kept your earth dollars safe.");
            break;
            case 1:
            this.greetingText.setText("Hey there! What do you want to do today?");
            break;
            case 2:
            this.greetingText.setText("Welcome. Let's get down to business.");
            break;
            case 3:
            this.greetingText.setText("Nice to see you again. Let's see how your wallet is doing.");
            break;
            case 4:
            this.greetingText.setText("Welcome back! Between you and me... your fly is undone.");
            break;
        }
    }
}
