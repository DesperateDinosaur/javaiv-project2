package com.digitalwallet;

public class Date {
    private int day;
    private int month;
    private int year;

    /**
     * constructor for standart date
     * @param d day
     * @param m month
     * @param y year
     */
    public Date(int d, int m, int y){
        this.day = d;
        this.month = m;
        this.year = y;
    }

    public Date(String dbDate){
        String[] dateParts = dbDate.split("/");
        this.month = Integer.parseInt(dateParts[1]);
        this.year = Integer.parseInt(dateParts[2]);
    }

    /**
     * copy constructor
     * @param expiryDate 
     */
    public Date(Date expiryDate) {
        this(expiryDate.getDay(), expiryDate.getMonth(), expiryDate.getYear());
    }

    // get and set methods
    public int getDay() {
        return this.day;
    }
    public int getMonth() {
        return this.month;
    }
    public int getYear() {
        return this.year;
    }

    /**
     * formats the date to be MM/YY
     * @return string representation of date
     */
    public String formatMMYY(){
        return month + "/" + year; 
    }

    /**
     * formats to be DD/MM/YY
     * @return string representation of date
     */
    public String formatDDMMYY(){
        return day + "/" + month + "/" + year;
    }
    
}