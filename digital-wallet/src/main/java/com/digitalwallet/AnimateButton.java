package com.digitalwallet;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * handles hover animation for buttons with backgrounds
 */
public class AnimateButton {

    /**
     * creates string properties for animation colors, binds them to css, creates timelines to animate colors, then creates event handlers to start and stop timelines
     * @param button
     * @param base
     * @param colorTwo
     */
    public static void animateButton(Button button, Color base, Color colorTwo) {
        final ObjectProperty<Color> animColor;
        final ObjectProperty<Color> baseColor;
        if (MainMenu.darkOn){
            animColor = new SimpleObjectProperty<>(base);
            baseColor = new SimpleObjectProperty<>(base);
        } else {
            animColor = new SimpleObjectProperty<>(base);
            baseColor = new SimpleObjectProperty<>(base);
        }
        final StringProperty animStringProperty = createButtonColorStringProperty(animColor);
        final StringProperty baseStringProperty = createButtonColorStringProperty(baseColor);

        button.styleProperty().bind(
                    new SimpleStringProperty("-fx-background-color: linear-gradient(to right, ")
                            .concat(baseStringProperty)
                            .concat(", ")
                            .concat(animStringProperty)
                            .concat(")")
            );

        Timeline active = createActive(animColor, base, colorTwo);
        Timeline off = createInactive(animColor, base, colorTwo);

        EventHandler buttonOn = e -> {
            button.setCursor(Cursor.HAND);
            off.stop();
            active.play();
        };

        EventHandler buttonOff = e -> {
            active.stop();
            off.play();
        };
        
        button.addEventHandler(MouseEvent.MOUSE_ENTERED, buttonOn);
        button.addEventHandler(MouseEvent.MOUSE_EXITED, buttonOff);
    }

    /**
     * creates a colorStringProperty and listens for changes on color ObjectProperty
     * @param animColor
     */
    private static StringProperty createButtonColorStringProperty(final ObjectProperty<Color> animColor) {
        final StringProperty colorStringProperty = new SimpleStringProperty();
        setColorStringFromColor(colorStringProperty, animColor);
        animColor.addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color oldColor, Color newColor) {
                setColorStringFromColor(colorStringProperty, animColor);
            }
        });

        return colorStringProperty;
    }

    /**
     * returns rgba string representation of java color for use in colorStringProperty
     * @param colorStringProperty
     * @param color
     */
    private static void setColorStringFromColor(StringProperty colorStringProperty, ObjectProperty<Color> color) {
        colorStringProperty.set(
                "rgba("
                        + ((int) (color.get().getRed()   * 255)) + ","
                        + ((int) (color.get().getGreen() * 255)) + ","
                        + ((int) (color.get().getBlue()  * 255)) + ","
                        + color.get().getOpacity() +
                ")"
        );
    }

    /**
     * create timeline for button on enter animation
     * @param animColor
     * @param colorOne
     * @param colorTwo
     */
    private static Timeline createActive(final ObjectProperty<Color> animColor, Color colorOne, Color colorTwo){
        Timeline time = new Timeline(
            new KeyFrame(Duration.millis(0), new KeyValue(animColor, colorOne, Interpolator.EASE_IN)),
            new KeyFrame(Duration.millis(250), new KeyValue(animColor, colorTwo, Interpolator.EASE_IN))
        );

        time.setCycleCount(1);
        time.setAutoReverse(false);

        return time;
    }

    /**
     * create timeline for button on exit animation
     * @param animColor
     * @param colorOne
     * @param colorTwo
     */
    private static Timeline createInactive(final ObjectProperty<Color> animColor, Color colorOne, Color colorTwo){
        Timeline time = new Timeline(
            new KeyFrame(Duration.millis(0), new KeyValue(animColor, colorTwo, Interpolator.EASE_OUT)),
            new KeyFrame(Duration.millis(200), new KeyValue(animColor, colorOne, Interpolator.EASE_OUT))
        );

        time.setCycleCount(1);
        time.setAutoReverse(false);

        return time;
    }
}
