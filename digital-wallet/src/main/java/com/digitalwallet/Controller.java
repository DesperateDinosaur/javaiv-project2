package com.digitalwallet;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.util.Duration;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * controls the main menu scene
 */
public class Controller implements Initializable{

    @FXML
    private Button btnMode;

    @FXML
    private Pane parent;

    @FXML
    private ImageView imgMode;

    @FXML
    private Button cardBtn;

    @FXML
    private Button cashBtn;

    @FXML
    private Button payBtn;

    @FXML
    private Button notesBtn;

    @FXML
    private ImageView imageSpin;

    @FXML
    private Pane paneSpin;

    @FXML
    private Button chooseImageBtn;

    @FXML
    private Label greetingText;

    @FXML
    private Button saveBtn;

    @FXML
    private Button loadBtn;

    @FXML
    private Label saveStatus;

    @FXML
    private Label loadStatus;


    static boolean isDarkMode = true;
    cardsController con = new cardsController();
    Database data = new Database();

    /**
     * sets the scene to cards
     * @param event
     * @throws IOException
     */
    @FXML
    public void switchToCards(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("cards");
    }

    /**
     * sets the scene to cash_view
     * @param event
     * @throws IOException
     */
    @FXML
    public void switchToCash(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("cash_view");
    }

    /**
     * sets the scene to notes_view
     * @param event
     * @throws IOException
     */
    @FXML
    public void switchToNotes(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("notes_view");
    }

    /**
     * sets the scene to pay_view
     * @param event
     * @throws IOException
     */
    @FXML
    public void switchToPay(ActionEvent event) throws IOException {
        MainMenu.scene.getStylesheets().remove(MainMenu.styleMode);
        MainMenu.scene.getStylesheets().add(MainMenu.styleMode);
        MainMenu.setRoot("pay_view");
    }

    /**
     * animates the image to spin at a consistent rate
     */
    @FXML
    public void makeImageSpin(){
        RotateTransition rt = new RotateTransition(Duration.millis(3000), imageSpin);
        rt.setByAngle(360);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.play();
    }

    /**
     * allows user to choose an image/gif to be spun
     * @param event
     */
    @FXML
    public void chooseImage(ActionEvent event){
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(new Stage());
        System.out.println(file.getAbsolutePath());
        // System.out.println(file.get);
        Image newImage = new Image("file:" + file.getAbsolutePath());
        imageSpin.setImage(newImage);
    }

    /**
     * animates the card button to have a fade animation
     */
    @FXML
    public void createAnimationCard() {
        final FadeTransition fadeIn = new FadeTransition(Duration.millis(300));
        fadeIn.setNode(cardBtn);
        fadeIn.setToValue(1);
        cardBtn.setOnMouseEntered(e -> {
            cardBtn.setCursor(Cursor.HAND);
            cardBtn.setStyle("-fx-background-color: radial-gradient(center 50% 50% , radius 200px , #ffebcd, #008080)");
            fadeIn.playFromStart();
        });

        final FadeTransition fadeOut = new FadeTransition(Duration.millis(300));
        fadeOut.setNode(cardBtn);
        // fadeOut.setToValue(0.5);
        cardBtn.setOnMouseExited(e -> {
            cardBtn.setCursor(Cursor.HAND);
            cardBtn.setStyle("-fx-background-color: #474056;");
            fadeOut.playFromStart();
        });

        cardBtn.setOpacity(1);
    }

    /**
     * changes the theme from dark mode to light mode
     * @param event
     */
    @FXML
    void changeTheme(ActionEvent event) {
        isDarkMode = !isDarkMode;
        if(isDarkMode){
            setDarkMode();
        }else{
            setLightMode();
        }
    }

    /**
     * sets the theme to dark and changes static variable
     */
    private void setDarkMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/dark_mode.css";
        MainMenu.darkOn = true;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/light_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * sets the theme to light and changes static variable
     */
    public void setLightMode() {
        MainMenu.styleMode = "file:src/main/resources/com/digitalwallet/style/light_mode.css";
        MainMenu.darkOn = false;
        parent.getStylesheets().remove("file:src/main/resources/com/digitalwallet/style/dark_mode.css");
        parent.getStylesheets().add("file:src/main/resources/com/digitalwallet/style/light_mode.css");
        buttonInitialize();
        Image img = new Image("file:src/main/resources/com/digitalwallet/images/dark_mode_btn.png");
        imgMode.setImage(img);
    }

    /**
     * initializes actions after FXML is loaded
     */
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        RotateTransition rt = new RotateTransition(Duration.millis(3000), imageSpin);
        rt.setByAngle(360);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.play();

        buttonInitialize();

        Greeting greetThread = new Greeting(greetingText);
        greetThread.start();
    }

    /**
     * initializes animations for buttons with backgrounds
     */
    private void buttonInitialize(){
        if (MainMenu.darkOn){
            AnimateButton.animateButton(cardBtn, Color.valueOf("#47354E"), Color.FORESTGREEN);
            AnimateButton.animateButton(cashBtn, Color.valueOf("#47354E"), Color.GOLDENROD);
            AnimateButton.animateButton(notesBtn, Color.valueOf("#47354E"), Color.DEEPSKYBLUE);
            AnimateButton.animateButton(payBtn, Color.valueOf("#47354E"), Color.SALMON);
            AnimateButton.animateButton(saveBtn, Color.valueOf("#145214"), Color.LIGHTGREEN);
            AnimateButton.animateButton(loadBtn, Color.valueOf("#1e44c0"), Color.DEEPSKYBLUE);

            chooseImageInitialize(true);
        } else {
            AnimateButton.animateButton(cardBtn, Color.valueOf("#AED1D1"), Color.LIGHTGREEN);
            AnimateButton.animateButton(cashBtn, Color.valueOf("#AED1D1"), Color.LIGHTGOLDENRODYELLOW);
            AnimateButton.animateButton(notesBtn, Color.valueOf("#AED1D1"), Color.SKYBLUE);
            AnimateButton.animateButton(payBtn, Color.valueOf("#AED1D1"), Color.LIGHTSALMON);
            AnimateButton.animateButton(saveBtn, Color.valueOf("#3fb13f"), Color.GREEN);
            AnimateButton.animateButton(loadBtn, Color.valueOf("#379cd6"), Color.SKYBLUE);
            chooseImageInitialize(false);
        }
    }

    /**
     * initializes animations for choose image button
     */
    private void chooseImageInitialize(Boolean dark){
        Timeline time;
        if (dark){
            time = new Timeline(
                new KeyFrame(Duration.millis(0), new KeyValue(chooseImageBtn.textFillProperty(), Color.WHITESMOKE, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(2000), new KeyValue(chooseImageBtn.textFillProperty(), Color.LAWNGREEN, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(4000), new KeyValue(chooseImageBtn.textFillProperty(), Color.SALMON, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(6000), new KeyValue(chooseImageBtn.textFillProperty(), Color.SKYBLUE, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(8000), new KeyValue(chooseImageBtn.textFillProperty(), Color.GOLD, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(10000), new KeyValue(chooseImageBtn.textFillProperty(), Color.WHITESMOKE, Interpolator.EASE_BOTH))
            );
        } else {
            time = new Timeline(
                new KeyFrame(Duration.millis(0), new KeyValue(chooseImageBtn.textFillProperty(), Color.GRAY, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(2000), new KeyValue(chooseImageBtn.textFillProperty(), Color.FORESTGREEN, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(4000), new KeyValue(chooseImageBtn.textFillProperty(), Color.CRIMSON, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(6000), new KeyValue(chooseImageBtn.textFillProperty(), Color.DARKCYAN, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(8000), new KeyValue(chooseImageBtn.textFillProperty(), Color.DARKGOLDENROD, Interpolator.EASE_BOTH)),
                new KeyFrame(Duration.millis(10000), new KeyValue(chooseImageBtn.textFillProperty(), Color.GRAY, Interpolator.EASE_BOTH))
            );
        }

        time.setCycleCount(Animation.INDEFINITE);
        time.setAutoReverse(false);
        time.play();
    }

    @FXML
    private void saveWallet() {
        data.save(MainMenu.notes, MainMenu.cards, 1, "wallet", MainMenu.cash);
        loadStatus.setText("");
        saveStatus.setText("Wallet saved successfully!");
    }

    @FXML
    private void loadWallet() {
        MainMenu.cash = data.load(MainMenu.notes, MainMenu.cards, MainMenu.btn);
        saveStatus.setText("");
        loadStatus.setText("Wallet loaded successfully!");
    }
}
