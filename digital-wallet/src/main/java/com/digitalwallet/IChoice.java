package com.digitalwallet;

public interface IChoice {
    public boolean deposit(double money);
    public boolean withdraw(double money);
}
