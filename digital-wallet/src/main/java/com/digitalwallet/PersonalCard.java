package com.digitalwallet;

/**
 * represents a personal card
 */
public class PersonalCard extends Card{
    
    /**
     * constructor
     * @param crdName name of the card
     * @param name name of card holder
     * @param num card number
     */
    public PersonalCard(String crdName, String name, String num){
        super(crdName, name, num, new Date(0, 0, 0));
    }

    /**
     * constructor that allows to add a date
     * @param crdName
     * @param name
     * @param num
     * @param date expiry date of the card
     */
    public PersonalCard(String crdName, String name, String num, Date date){
        super(crdName, name, num, date);
    }

    /**
     * copy constructor
     * @param card
     */
    public PersonalCard(PersonalCard card){
        super(card.getCrdName(), card.getName(), card.getNumber(), card.getExpiryDate());
    }   
}