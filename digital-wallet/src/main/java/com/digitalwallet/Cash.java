package com.digitalwallet;

/**
 * an object used to keep track of how much cash a user has
 * can withdraw and deposit cash
 */
public class Cash implements IChoice{
  private Double cash;

  /**
   * constructor
   * @param cash
   */
  public Cash(double cash){
    this.cash = cash;
  }

  /**
   * deposits cash into account
   * aka adds more to the amount and rounds it to a 2 decimal double
   * @param amount
   */
  public boolean deposit(double amount){
    this.cash += Math.round(amount * 100.0)/100.0;
    this.cash = Math.round(this.cash * 100.0)/100.0;
    System.out.println(this.cash);
    return true;
  }

  /**
   * removes amount from cash if and only if cash is greater than amount
   * @param amount
   * @return boolean if action was successful
   */
  public boolean withdraw(double amount){
    if (this.cash >= amount){
      this.cash -= Math.round(amount * 100.0)/100.0;
      return true;
    }
    else {
      return false;
    }
  }

  public double getCash() {
    return this.cash;
  }

  public String toString(){
    return "$" + this.cash;
  }
}
