/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.digitalwallet;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class CardTest {
    
    public CardTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Card.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Card instance = new Card("Gift Card", "Card Internoscia", "12345", new Date(12, 5, 2023));
        String expResult = "Card Internoscia";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumber method, of class Card.
     */
    @Test
    public void testGetNumber() {
        System.out.println("getNumber");
        Card instance = new Card("Gift Card", "Card Internoscia", "12345", new Date(12, 5, 2023));
        String expResult = "12345";
        String result = instance.getNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getExpiryDate method, of class Card.
     */
    @Test
    public void testGetExpiryDate() {
        System.out.println("getExpiryDate");
        Date date = new Date(12, 5, 2023);
        Card instance = new Card("Gift Card", "Card Internoscia", "12345", date);
        int expDay = 12;
        int expMonth = 5;
        int expYear = 2023;
        Date result = instance.getExpiryDate();
        assertEquals(expDay, result.getDay());
        assertEquals(expMonth, result.getMonth());
        assertEquals(expYear, result.getYear());
    }
    
}
