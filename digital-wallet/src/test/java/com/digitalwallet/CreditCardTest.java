/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.digitalwallet;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class CreditCardTest {
    
    public CreditCardTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getAmount method, of class CreditCard.
     */
    // @Test
    // public void testGetAmount() {
    //     System.out.println("getAmount");
    //     CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
    //     double expResult = 32.0;
    //     double result = instance.getAmount();
    //     assertEquals(expResult, result, 0.0);
    // }

    /**
     * Test of getBank method, of class CreditCard.
     */
    // @Test
    // public void testGetBank() {
    //     System.out.println("getBank");
    //     CreditCard instance = null;
    //     Bank expResult = null;
    //     Bank result = instance.getBank();
    //     assertEquals(expResult, result);
    //     // fail("The test case is a prototype.");
    // }

    /**
     * Test of getLimit method, of class CreditCard.
     */
    // @Test
    // public void testGetLimit() {
    //     System.out.println("getLimit");
    //     CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
    //     double expResult = 64.0;
    //     double result = instance.getLimit();
    //     assertEquals(expResult, result, 0.0);
    //     // fail("The test case is a prototype.");
    // }

    /**
     * Test of getThreeDigits method, of class CreditCard.
     */
    @Test
    public void testGetThreeDigits() {
        System.out.println("getThreeDigits");
        CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
        int expResult = 123;
        int result = instance.getThreeDigits();
        assertEquals(expResult, result);
    }

    /**
     * Test of deposit method false, of class CreditCard.
     */
    @Test
    public void testDepositFalse() {
        System.out.println("deposit");
        double amount = 50000.0;
        CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
        boolean expResult = false;
        boolean result = instance.deposit(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of deposit method true, of class CreditCard.
     */
    @Test
    public void testDepositTrue() {
        System.out.println("deposit");
        double amount = 10.0;
        CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
        boolean expResult = true;
        boolean result = instance.deposit(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of withdraw method false, of class CreditCard.
     */
    @Test
    public void testWithdrawFalse() {
        System.out.println("withdraw");
        double amount = 128.0;
        CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
        boolean expResult = false;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of withdraw method true, of class CreditCard.
     */
    @Test
    public void testWithdrawTrue() {
        System.out.println("withdraw");
        double amount = 32.0;
        CreditCard instance = new CreditCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123);
        boolean expResult = true;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }
    
}
