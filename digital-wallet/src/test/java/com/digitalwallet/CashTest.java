/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.digitalwallet;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class CashTest {
    
    public CashTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of deposit method, of class Cash.
     */
    @Test
    public void testDeposit() {
        System.out.println("deposit");
        Double amount = 500.00;
        Cash instance = new Cash(500.00);
        instance.deposit(amount);
    }

    /**
     * Test of withdraw method false, of class Cash.
     */
    @Test
    public void testWithdrawFalse() {
        System.out.println("withdraw");
        Double amount = 600.00;
        Cash instance = new Cash(500.00);
        boolean expResult = false;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of withdraw method true, of class Cash.
     */
    @Test
    public void testWithdrawTrue() {
        System.out.println("withdraw");
        Double amount = 400.00;
        Cash instance = new Cash(500.00);
        boolean expResult = true;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCash method, of class Cash.
     */
    @Test
    public void testGetCash() {
        System.out.println("getCash");
        Cash instance = new Cash(500.00);
        double expResult = 500.0;
        double result = instance.getCash();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Cash.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cash instance = new Cash(500.00);
        String expResult = "$500.0";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
