/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.digitalwallet;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class DebitCardTest {
    
    public DebitCardTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getBalance method, of class DebitCard.
     */
    @Test
    public void testGetBalance() {
        System.out.println("getBalance");
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        double expResult = 50.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getBank method, of class DebitCard.
     */
    // @Test
    // public void testGetBank() {
    //     System.out.println("getBank");
    //     Bank bank = new Bank();
    //     DebitCard instance = new DebitCard("DebitCard Internoscia", 12345, new Date(12, 5, 2023), 123, 50.0, bank);
    //     Bank expResult = bank;
    //     Bank result = instance.getBank();
    //     assertEquals(expResult, result);
    //     // fail("The test case is a prototype.");
    // }

    /**
     * Test of getThreeDigits method, of class DebitCard.
     */
    @Test
    public void testGetThreeDigits() {
        System.out.println("getThreeDigits");
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        int expResult = 123;
        int result = instance.getThreeDigits();
        assertEquals(expResult, result);
    }

    /**
     * Test of checkFunds method true, of class DebitCard.
     */
    @Test
    public void testCheckFundsTrue() {
        System.out.println("checkFunds");
        int amount = 25;
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        boolean expResult = true;
        boolean result = instance.checkFunds(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkFunds method false, of class DebitCard.
     */
    @Test
    public void testCheckFundsFalse() {
        System.out.println("checkFunds");
        int amount = 75;
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        boolean expResult = false;
        boolean result = instance.checkFunds(amount);
        assertEquals(expResult, result);
    }

    /**
     * Test of withdraw method true, of class DebitCard.
     */
    @Test
    public void testWithdrawTrue() {
        System.out.println("withdraw");
        int amount = 25;
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        boolean expResult = true;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }

/**
     * Test of withdraw method false, of class DebitCard.
     */
    @Test
    public void testWithdrawFalse() {
        System.out.println("withdraw");
        int amount = 75;
        DebitCard instance = new DebitCard("Card name", "Internoscia", "12345", new Date(12, 5, 2023), 123, 50.0);
        boolean expResult = false;
        boolean result = instance.withdraw(amount);
        assertEquals(expResult, result);
    }
    
}
