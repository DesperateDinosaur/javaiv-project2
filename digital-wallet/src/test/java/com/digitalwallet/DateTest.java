/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.digitalwallet;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class DateTest {
    
    public DateTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getDay method, of class Date.
     */
    @Test
    public void testGetDay() {
        System.out.println("getDay");
        Date instance = new Date(20, 4, 1969);
        int expResult = 20;
        int result = instance.getDay();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMonth method, of class Date.
     */
    @Test
    public void testGetMonth() {
        System.out.println("getMonth");
        Date instance = new Date(20, 4, 1969);
        int expResult = 4;
        int result = instance.getMonth();
        assertEquals(expResult, result);
    }

    /**
     * Test of getYear method, of class Date.
     */
    @Test
    public void testGetYear() {
        System.out.println("getYear");
        Date instance = new Date(20, 4, 1969);
        int expResult = 1969;
        int result = instance.getYear();
        assertEquals(expResult, result);
    }

    /**
     * Test of formatMMYY method, of class Date.
     */
    @Test
    public void testFormatMMYY() {
        System.out.println("formatMMYY");
        Date instance = new Date(20, 4, 1969);
        String expResult = "4/1969";
        String result = instance.formatMMYY();
        assertEquals(expResult, result);
    }

    /**
     * Test of formatDDMMYY method, of class Date.
     */
    @Test
    public void testFormatDDMMYY() {
        System.out.println("formatDDMMYY");
        Date instance = new Date(20, 4, 1969);
        String expResult = "20/4/1969";
        String result = instance.formatDDMMYY();
        assertEquals(expResult, result);
    }
    
}
