package com.digitalwallet;

import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * used to remind the user if set
 * runs on its own thread
 */
public class Reminder implements Runnable {
  private Stage stage;
  private Thread t;
  private String threadName;
  private String frequency;
  private double amount;
  private boolean repeat;
  private boolean interrupt;

  /**
   * constructor
   * @param frequency representation of minutes hours days of reminder
   * @param amount amount to be reminded dependent on frequency
   * @param repeat if repeatble
   * @param name name of reminder
   */
  public Reminder(String frequency, double amount, boolean repeat, String name){
    this.stage = stage;
    this.threadName = name;
    this.frequency = frequency;
    this.amount = amount;
    this.repeat = repeat;
  }

  /**
   * shows dialog prompt of reminder
   */
  public void initDialog(){
    //Creating a dialog
    Dialog<String> dialog = new Dialog<String>();
    //Setting the title
    dialog.setTitle("Dialog");
    ButtonType type = new ButtonType("Ok", ButtonData.OK_DONE);
    //Setting the content of the dialog
    dialog.setContentText("This is a sample dialog");
    //Adding buttons to the dialog pane
    dialog.getDialogPane().getButtonTypes().add(type);
    //Setting the label
    Text txt = new Text("Click the button to show the dialog");
    //Creating a button
    Button button = new Button("Show Dialog");
    //Creating a vbox to hold the button and the label
    HBox pane = new HBox(15);
    //Setting the space between the nodes of a HBox pane
    pane.setPadding(new Insets(50, 150, 50, 60));
    pane.getChildren().addAll(txt, button);
    //Creating a scene object
    Stage notifStage = new Stage();
    dialog.initModality(Modality.APPLICATION_MODAL);
    dialog.initOwner(stage);
    Scene scene = new Scene(new Group(pane), 595, 250, Color.BEIGE);
    notifStage.setTitle("Dialog");
    notifStage.setScene(scene);
    notifStage.show();
  }

  /**
   * runs the reminder on a thread
   */
  public void run() {
    System.out.println("This is the amount before adjusting: " + amount);
    // check unit of time to wait in frequency and adjust time amount
    if (frequency.equals("minutes")){
      amount = amount*60000;
    } else if (frequency.equals("hours")){
      amount = amount*3600000;
    } else {
      amount = amount*86400000;
    }

    System.out.println(threadName + " thread set to repeat every " + amount + " milliseconds");

    while (repeat == true){
      if (!interrupt){
        try {
          System.out.println("Waiting " + amount + " milliseconds...");
          Thread.currentThread().sleep((int)amount);
          System.out.println("Repeating " + threadName + " Notification");
          initDialog();
        } catch (Exception e) {
          //ignore exception
        }
      }
    }

    if (!repeat){
      if (!interrupt){
        try {
          System.out.println("Waiting " + amount + " milliseconds...");
          Thread.currentThread().sleep((int)amount);
          System.out.println("Non-repeating " + threadName + " notification");
          initDialog();
        } catch (Exception e) {
          //ignore exception
        }
      }
    }

    System.out.println("Thread " + threadName + " exiting...");
  }

  /**
   * cancles the reminder
   */
  public void cancel() {
    try {
      System.out.println("Notification " + threadName + " Cancelled");
      interrupt = true;
      t.interrupt();
    } catch (Exception e) {
      //ignore exception
    }
  }

  /**
   * starts the thread
   */
  public void start() {
    System.out.println("Starting " + threadName);
    if (t == null){
      t = new Thread (this, threadName);
      t.start();
    }
  }
}