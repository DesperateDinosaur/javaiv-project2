public class Note_reminder_code {
    private Reminder remindThread;

    /**
   * sets a reminder for the note
   * @param frequency how many times to repeat reminder
   * @param amount length until reminded
   * @param repeat if the reminder is repeatable
   */
  public void setReminder(String frequency, double amount, boolean repeat){
    // this.isReminder = true;
    this.remindThread = new Reminder(frequency, amount, repeat, title);
    System.out.println("Reminder created on note " + id);
    try {
      System.out.println("Running reminder thread...");
      this.remindThread.run();
    } catch (Exception e) {
      //ignore exception
    }
  }

  /**
   * deletes the reminder
   */
  public void delReminder(){
    this.remindThread.cancel();
    // isReminder = false;
  }
}
