# Project 2: Plan and Check List

## Items in eWallet
<hr>

*Wallet has a maximum of 10 cards and 10 notes*

### Credit Card
- Credit card have a limit
  - ranges from 100 - 5000 $
- When the user reaches 50% of the limit:
  - The wallet gives a warning
  - User should not exceed 50%
- Following attributes:
  - Card holder name
  - Card number
  - Expiry date (mm/yy)
  - Three digits in the back

### Debit Card
- Attached to a bank account
  - Limit is the same as funds in bank account
- Must create a mock object to simulate the functionality of bank
- Two Methods:
  - boolean checkFundsAvailibility(int amount) <br> 
  returns true if balance is greater or equal to amount
  - boolean withdraw(int amount) <br> balance - amount only if theres enough funds <br> returns true if operation was successful
- Following attributes:
  - Card holder name
  - Card number
  - Expiry date (mm/yy)
  - Three digits in the back

### Cash

### Personal Cards
- Health care card, drivers license, etc
- Following Attributes:
  - Card holder name
  - Card number
  - *Optional: Expiry date*

### Notes
- Following attributes:
  - Creation date
  - Body
  - *Optional: Can Issue a reminder*
    - If reminder is set,frequency of reminder is set (minutes,hours,days)
    - Can be a sound, message or whatever else

-----

## Operations of the wallet

1) Add a card
     - Adds any of the valid cards to the wallet
2) Delete a card
    - Delete any valid card from the wallet
3) Pay
    - Using any of the valid payment methods (Cash, card, credit) subtract payment amount from method
4) Browse through the notes
   - List all written notes in wallet
5) Delete a note
   - Delete a sigular note
6) Save e-wallet to DB
   - Save current information to a DB (mock object or file whatever he wasnt clear)
7) Load e-wallet from DB
   - Load a saved wallet
8) Display how much cash we have
   - Display amounts of money in cash left 
9)  Add cash
    - Add more to cash

<hr>

## 3-Layer Architecture

1) Presentation --> JavaFX, GUI
2) Application --> Bussiness logic
3) Data --> Database

<hr>

## MVC

Must seperate the **Model** from the **View** <br>
**Controller** Is the bridge between them 

**Model** --> Data structure, eg. Updates application to reflect added item
   - Updates **View** 

**View** --> Display (UI), eg. User clicks add to cart
- Sends input from user to **Controller**

**Controller** --> Control logic, eg. Receives from view then notifies model to add item
- Manipulates **Model**
- Sometimes updates directly the **View**

<hr>

## Specific requirements

- Java with Maven
- Unit tests
- Gitlab
- MVC
- Gui on its own thread
- Business logic on its own thread
- Observer pattern
- Database (Mock object)
- Display favorite picture (Use animation to make it move)

### Submission

- UML
- Sample runs
- Source code