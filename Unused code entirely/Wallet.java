package com.digitalwallet;

import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * early version of the wallet application before scene builder
 */
public class Wallet extends Application{

    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Digital Wallet");
        VBox container = new VBox();

        Note note = new Note();
        note.setBody("Title\n");
        System.out.println(note.getTitle());
        System.out.println("------- divider between title and body -------");
        System.out.println(note.getBody());
        try {
            note.outputFile();
        } catch (Exception e) {
            System.out.println(e);
        }
        //note.setReminder("minutes", 0.1, true);

        HBox walletActions = new HBox();
        walletActions.setAlignment(Pos.CENTER);
        
        VBox money = new VBox();
        VBox notes = new VBox();
        VBox wallet = new VBox();

        money.setMinWidth(200);
        notes.setMinWidth(200);
        wallet.setMinWidth(200);

        money.setId("money");
        notes.setId("notes");
        wallet.setId("wallet");
        walletActions.setId("walletActions");

        Label ca = new Label("Cash");
        ca.setId("ca");

        Button addCard = createButton("Add new card", Color.LAWNGREEN, note);
        Button delCard = createButton("Delete a card", Color.LAWNGREEN, note);
        Button addCash = createButton("Add cash", Color.LAWNGREEN, note);
        Button balance = createButton("Check balance", Color.LAWNGREEN, note);
        Button pay = createButton("Pay", Color.LAWNGREEN, note);

        money.getChildren().addAll(ca, addCard, delCard, addCash, balance, pay);

        Label no = new Label("Notes");
        no.setId("no");

        Button seeNotes = createButton("Browse notes", Color.LIGHTSALMON, note);
        Button delNote = createButton("Delete a note", Color.LIGHTSALMON, note);

        notes.getChildren().addAll(no, seeNotes, delNote);

        Label wa = new Label("Wallet");
        wa.setId("wa");

        Button saveWall = createButton("Save e-wallet", Color.SKYBLUE, note);
        Button loadWall = createButton("Load e-wallet", Color.SKYBLUE, note);

        wallet.getChildren().addAll(wa, saveWall, loadWall);
        walletActions.getChildren().addAll(money, notes, wallet);

        HBox walletBottom = new HBox();
        walletBottom.setAlignment(Pos.CENTER);

        HBox walletDisplay = new HBox();
        walletDisplay.setMinWidth(600);

        VBox walletImg = new VBox();
        VBox walletOne = new VBox();
        VBox walletTwo = new VBox();

        walletImg.setMinWidth(138);
        walletOne.setMinWidth(228);
        walletTwo.setMinWidth(228);

        walletImg.setId("walletImg");
        walletOne.setId("walletOne");
        walletTwo.setId("walletTwo");
        walletDisplay.setId("walletDisplay");
        walletBottom.setId("walletBottom");

        Image walletPng = new Image(getClass().getResource("resources/images/img.png").toExternalForm());
        ImageView walletView = new ImageView();
        walletView.setImage(walletPng);

        walletImg.getChildren().addAll(walletView);

        Button sample11 = createButton("Card 1", Color.GOLD, note);
        Button sample12 = createButton("Card 2", Color.GOLD, note);
        Button sample13 = createButton("Card 3", Color.GOLD, note);
        Button sample14 = createButton("Card 4", Color.GOLD, note);
        Button sample15 = createButton("Card 5", Color.GOLD, note);

        walletOne.getChildren().addAll(sample11, sample12, sample13, sample14, sample15);

        Text sample21 = new Text("Card information 1");
        Text sample22 = new Text("Card information 2");
        Text sample23 = new Text("Card information 3");

        walletTwo.getChildren().addAll(sample21, sample22, sample23);

        walletDisplay.getChildren().addAll(walletImg, walletOne, walletTwo);
        walletBottom.getChildren().addAll(walletDisplay);

        Popup notif = new Popup();
        Label notifText = new Label("Test reminder");
        Button goToNote = new Button("Reminder button");
        notif.getContent().addAll(notifText, goToNote);

        container.getChildren().addAll(walletActions, walletBottom);

        StackPane root = new StackPane(container);
        Scene scene = new Scene(root, 675, 400);

        scene.getStylesheets().add(getClass().getResource("resources/css/styles.css").toExternalForm());
        this.primaryStage.setScene(scene);
        this.primaryStage.getIcons().add(new Image(getClass().getResource("resources/images/icon.png").toExternalForm()));
        this.primaryStage.show();
        notif.show(this.primaryStage);


    }

    private StringProperty createButtonColorStringProperty(final ObjectProperty<Color> animColor) {
        final StringProperty colorStringProperty = new SimpleStringProperty();
        setColorStringFromColor(colorStringProperty, animColor);
        animColor.addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observableValue, Color oldColor, Color newColor) {
                setColorStringFromColor(colorStringProperty, animColor);
            }
        });

        return colorStringProperty;
    }

    private void setColorStringFromColor(StringProperty colorStringProperty, ObjectProperty<Color> color) {
        colorStringProperty.set(
                "rgba("
                        + ((int) (color.get().getRed()   * 255)) + ","
                        + ((int) (color.get().getGreen() * 255)) + ","
                        + ((int) (color.get().getBlue()  * 255)) + ","
                        + color.get().getOpacity() +
                ")"
        );
    }

    private Button createButton(String label, Color color, Note note) {
        final ObjectProperty<Color> animColor = new SimpleObjectProperty<>(Color.DARKSLATEGRAY);
        final StringProperty colorStringProperty = createButtonColorStringProperty(animColor);

        final Button warningButton = new Button(label);
        warningButton.styleProperty().bind(
                new SimpleStringProperty("-fx-base: ")
                        .concat(colorStringProperty)
        );

        Timeline active = createActive(animColor, color);
        Timeline off = createInactive(animColor, color);
        Timeline click = createClick(animColor, color);

        EventHandler buttonOn = e -> {
            off.stop();
            click.stop();
            active.play();
        };

        EventHandler buttonOff = e -> {
            active.stop();
            click.stop();
            off.play();
        };

        EventHandler buttonClick = e -> {
            off.stop();
            click.play();
            active.play();
        };

        EventHandler buttonEnter = e -> {
            click.play();
            off.setDelay(Duration.millis(300));
            off.play();
            off.setDelay(Duration.millis(0));
        };

        warningButton.setOnMouseEntered(buttonOn);
        warningButton.setOnMouseExited(buttonOff);
        warningButton.setOnMouseClicked(buttonClick);
        warningButton.setOnAction(buttonEnter);

        return warningButton;
    }

    private Timeline createActive(final ObjectProperty<Color> animColor, Color color){
        Timeline time = new Timeline(
            new KeyFrame(Duration.millis(0), new KeyValue(animColor, Color.DARKSLATEGRAY, Interpolator.EASE_BOTH)),
            new KeyFrame(Duration.millis(50), new KeyValue(animColor, color, Interpolator.EASE_BOTH))
        );

        time.setCycleCount(1);
        time.setAutoReverse(false);

        return time;
    }

    private Timeline createInactive(final ObjectProperty<Color> animColor, Color color){
        Timeline time = new Timeline(
            new KeyFrame(Duration.millis(0), new KeyValue(animColor, color, Interpolator.EASE_BOTH)),
            new KeyFrame(Duration.millis(50), new KeyValue(animColor, Color.DARKSLATEGRAY, Interpolator.EASE_BOTH))
        );

        time.setCycleCount(1);
        time.setAutoReverse(false);

        return time;
    }

    private Timeline createClick(final ObjectProperty<Color> animColor, Color color){
        Timeline time = new Timeline(
            new KeyFrame(Duration.millis(200), new KeyValue(animColor, Color.DARKSLATEGRAY, Interpolator.EASE_BOTH)),
            new KeyFrame(Duration.millis(300), new KeyValue(animColor, color, Interpolator.EASE_BOTH))
        );

        time.setCycleCount(1);
        time.setAutoReverse(false);

        return time;
    }
}
